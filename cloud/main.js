/*global Parse */
/*jslint node: true */
/*jslint nomen: true */
var Ingame = {
    Error: {
        MISSING_PARAMS: 5000,
        UNSUPPORTED_GAME: 5001,
        NEVER_PLAY_GAME: 5002,
        UNSUPPORTED_REGION: 5003,
        UNAUTHORIZED_REQUEST: 5004
    },
    AppIds: {
        "ru": "0ba576f27f3b0dc95cd56bf18733b1f1",
        "asia": "31660921fc05fd4a81c273238d16b9d2",
        "eu": "ebf53a8e1d483cd16166e0538f5beff6",
        "na": "0391a11c5fa1966d5aed615427834e92",
        "kr": "30d648c15db90131fc147c16a377d65e"
    },
    URLs: {
        "ru": "https://api.worldoftanks.ru/",
        "eu": "https://api.worldoftanks.eu/",
        "na": "https://api.worldoftanks.com/",
        "asia": "https://api.worldoftanks.asia/",
        "kr": "https://api.worldoftanks.kr/"
    },
    WOWPURLs: {
        "ru": "https://api.worldofwarplanes.ru/",
        "eu": "https://api.worldofwarplanes.eu/",
        "na": "https://api.worldofwarplanes.com/",
        "asia": "https://api.worldofwarplanes.asia/",
        "kr": "https://api.worldofwarplanes.kr/"
    },
    getNickname: function (nickname, game, region) {
        "use strict";
        return region + "_" + "wot" + "_" + nickname;
    },
    generatePassword: function () {
        "use strict";
        var length = 50,
            charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_[];-",
            retVal = "",
            i,
            n;
        for (i = 0, n = charset.length; i < length; i += 1) {
            retVal += charset.charAt(Math.random() * n);
        }
        return retVal;
    },
    createNewUser: function (nickname, game, region, active) {
        "use strict";
        Parse.Cloud.useMasterKey();
        var password = Ingame.generatePassword(),
            user = new Parse.User();
        user.setUsername(Ingame.getNickname(nickname, "wot", region));
        user.setPassword(password);
        user.set("wotNickname", Ingame.getNickname(nickname, "wot", region));
        if (typeof active !== "undefined") {
            user.set("active", true);
        }
        return user.save().then(function () {
            return Parse.Promise.as({
                password: password,
                username: user.getUsername(),
                user: user
            });
        });
    },
    getPushData: function (nickname, message, game) {
        "use strict";
        return {
            action: "com.appgranula.ingame.INGAME_MESSAGE",
            aps: {
                alert: message,
                sound: "default"
            },
            sender: {
                nickname: nickname,
                game: game
            }
        };
    }
};
Parse.Cloud.define("wargamingAuth", function (request, response) {
    "use strict";
    var token = request.params.accessToken,
        accountId = request.params.accountId,
        region = request.params.region,
        nickname,
        applicationId = Ingame.AppIds[region],
        url = Ingame.URLs[region] + "wot" + "/account/info/?application_id=" + applicationId + "&account_id=" + accountId,
        urlWowp = Ingame.WOWPURLs[region] + "wowp" + "/account/info/?application_id=" + applicationId + "&account_id=" + accountId,
        options = {
            url: url
        },
        game = request.params.game;
    if (!applicationId) {
        response.error(new Parse.Error(Ingame.Error.UNSUPPORTED_REGION, "Unsupported region: " + region));
        return;
    }
    if (!token || !accountId) {
        response.error(new Parse.Error(Ingame.Error.MISSING_PARAMS, "Required params: accessToken, accountId"));
        return;
    }
    return Parse.Cloud.httpRequest(options).then(function (result) {
        if (result.status === 200 && result.data.status === "ok") {
            var userInfo = result.data.data[accountId],
                queryUser = new Parse.Query(Parse.User);
            if (!userInfo) {
                options.url = urlWowp;
                return Parse.Cloud.httpRequest(options).then(function (wowpResult) {
                    userInfo = wowpResult.data.data[accountId];
                    if (!userInfo) {
                        return Parse.Promise.error(new Parse.Error(Ingame.Error.NEVER_PLAY_GAME, "User never play wot or wowp"));
                    }
                    if (userInfo.nickname) {
                        nickname = userInfo.nickname;
                        Parse.Cloud.useMasterKey();
                        queryUser.equalTo("wotNickname", Ingame.getNickname(nickname, game, region));
                        return queryUser.first();
                    } else {
                        return Parse.Promise.error(new Parse.Error(Parse.Error.VALIDATION_ERROR, "Access token, account id, or nickname are invalid"));
                    }
                });
            } else {
                if (userInfo.nickname) {
                    nickname = userInfo.nickname;
                    Parse.Cloud.useMasterKey();
                    queryUser.equalTo("wotNickname", Ingame.getNickname(nickname, game, region));
                    return queryUser.first();
                } else {
                    return Parse.Promise.error(new Parse.Error(Parse.Error.VALIDATION_ERROR, "Access token, account id, or nickname are invalid"));
                }
            }
        } else {
            return Parse.Promise.error(new Parse.Error(Parse.Error.VALIDATION_ERROR, "Access token, account id, or nickname are invalid"));
        }
    }).then(function (user) {
        if (!user) {
            return Ingame.createNewUser(nickname, game, region, true);
        } else {
            Parse.Cloud.useMasterKey();
            var password = Ingame.generatePassword();
            user.setPassword(password);
            user.set("active", true);
            return user.save().then(function () {
                return Parse.Promise.as({
                    password: password,
                    username: user.getUsername()
                });
            });
        }
    }).then(function (usercredentials) {
        response.success(usercredentials);
    }, function (error) {
        console.log("ERROR: " + JSON.stringify(error));
        response.error(error);
    });
});

Parse.Cloud.define("logout", function (request, response) {
    "use strict";
    var currentUser = Parse.User.current(),
        installationId = request.installationId,
        query = new Parse.Query("_Installation");

    if (!currentUser) {
        return response.error(new Parse.Error(Ingame.Error.UNAUTHORIZED_REQUEST, "Unauthorized request"));
    }

    query.equalTo("installationId", installationId);

    Parse.Cloud.useMasterKey();
    return query.first().then(function (installation) {
        installation.unset("wotNickname");
        return installation.save();
    }).then(function () {
        var query = new Parse.Query("_Installation");
        query.equalTo("wotNickname", currentUser.get("wotNickname"));
        return query.first();
    }).then(function (anyOther) {
        if (anyOther) {
            return Parse.Promise.as();
        } else {
            currentUser.unset("active");
            return currentUser.save();
        }
    }).then(function () {
        response.success();
    }, function (error) {
        response.error(error);
    });
});

Parse.Cloud.define("updateOnlineStatus", function (request, response) {
    "use strict";
    var nickname = request.params.nickname,
        game = request.params.game,
        onlineStatus = request.params.onlineStatus,
        region = request.params.region.toLocaleLowerCase(),
        userQuery = new Parse.Query(Parse.User),
        firstTime,
        moment = require('moment'),
        regionNickname;
    if (!nickname) {
        return response.error(new Parse.Error(Parse.Error.VALIDATION_ERROR, "Empty nickname field"));
    }
    if (game !== "wot" && game !== "wowp") {
        return response.error(new Parse.Error(Ingame.Error.UNSUPPORTED_GAME, "Unsupported game: " + game));
    }
    Parse.Cloud.useMasterKey();
    regionNickname = Ingame.getNickname(nickname, game, region);
    userQuery.equalTo("wotNickname", regionNickname);
    return userQuery.first().then(function (user) {
        if (!user) {
            return Ingame.createNewUser(nickname, game, region);
        }
        return Parse.Promise.as({
            user: user
        });
    }).then(function (userData) {
        var user = userData.user,
            lastOnlineTime = user.get(game + "Online");
        firstTime = moment().diff(lastOnlineTime) > 1000 * 60 * 15;
        if (onlineStatus) {
            user.set(game + "Online", new Date());
        } else {
            user.unset(game + "Online");
        }
        return user.save();
    }).then(function (user) {
        if (!firstTime) {
            return Parse.Promise.as();
        }
        var queryUsers = new Parse.Query(Parse.User),
            queryInstallations = new Parse.Query("_Installation"),
            gameName = (game === "wot") ? "World of Tanks" : "World of Warplanes",
            message = (region === "ru") ? (nickname + " в " + gameName) : nickname + " in " + gameName;

        queryUsers.equalTo("active", true);
        queryUsers.equalTo("watchList", user.get("wotNickname"));
        queryInstallations.matchesKeyInQuery("wotNickname", "wotNickname", queryUsers);
        return Parse.Push.send({
            where: queryInstallations,
            data: Ingame.getPushData(regionNickname, message, game)
        });
    }).then(function () {
        response.success({
            status: "ok"
        });
    }, function (error) {
        response.error(error);
    });
});

Parse.Cloud.define("sendPush", function (request, response) {
    "use strict";
    var currentUser = Parse.User.current(),
        nicknames = request.params.nicknames,
        message = request.params.message,
        game = request.params.game,
        queryUsers,
        queryUsersWot = new Parse.Query(Parse.User),
        queryUsersWowp = new Parse.Query(Parse.User),
        queryUsersClan = new Parse.Query(Parse.User),
        queryOld = new Parse.Query(Parse.User),
        queryInstallations = new Parse.Query("_Installation"),
        IngameLogger = Parse.Object.extend("IngameLoger"),
        ingameLoggers = [],
        ingameLogger,
        userNickname = currentUser.get("wotNickname");
    if (!currentUser) {
        return response.error(new Parse.Error(Ingame.Error.UNAUTHORIZED_REQUEST, "Unauthorized request"));
    }
    if (!nicknames) {
        return response.error(new Parse.Error(Parse.Error.VALIDATION_ERROR, "Empty nicknames field"));
    }
    if (game !== "wot" && game !== "wowp") {
        return response.error(new Parse.Error(Ingame.Error.UNSUPPORTED_GAME, "Unsupported game: " + game));
    }
    Parse.Cloud.useMasterKey();
    if (!currentUser.get("wotFriends") && !currentUser.get("wowpFriends") && !currentUser.get("wotClanMembers")) {
        queryUsers = new Parse.Query(Parse.User);
    } else {
        queryUsersWot.equalTo("wotFriends", userNickname);
        queryUsersWowp.equalTo("wowpFriends", userNickname);
        queryUsersClan.equalTo("wotClanMembers", userNickname);
        queryOld.doesNotExist("wotFriends");
        queryOld.doesNotExist("wowpFriends");
        queryOld.doesNotExist("wotClanMembers");
        queryUsers = Parse.Query.or(queryUsersWot, queryUsersWowp, queryUsersClan, queryOld);
    }

    queryUsers.equalTo("active", true);

    queryInstallations.matchesKeyInQuery("wotNickname", "wotNickname", queryUsers);
    queryInstallations.containedIn("wotNickname", nicknames);


    nicknames.forEach(function (entry) {
        ingameLogger = new IngameLogger();
        ingameLogger.set("from", currentUser.get("wotNickname"));
        ingameLogger.set("to", entry);
        ingameLoggers.push(ingameLogger);
    });


    return Parse.Object.saveAll(ingameLoggers).then(function () {
        return Parse.Push.send({
            where: queryInstallations,
            data: Ingame.getPushData(Parse.User.current().get("wotNickname"), message, game)
        });
    }).then(function () {
        response.success();
    }, function (error) {
        response.error(error);
    });
});

Parse.Cloud.job("updateOrphans", function (request, status) {
    "use strict";
    var moment = require('moment');
    var query = new Parse.Query("_Installation");
    var dateLimit = moment().subtract({
        d: 14
    }).toDate();
    var installationNumber = 0,
        userNumber = 0;

    Parse.Cloud.useMasterKey();

    console.log("Drop installations older then: " + dateLimit);

    query.lessThan("lastSeenAt", dateLimit);
    query.exists("wotNickname");
    query.notEqualTo("wotNickname", "");
    query.find().then(function (orphans) {

        if (typeof (orphans) === undefined || orphans.length === 0) {
            return Parse.Promise.as();
        }

        status.message("Processing installations");
        installationNumber = orphans.length;
        var promises = orphans.map(function (installation) {
            installation.unset("wotNickname");
            return installation.save();
        });

        return Parse.Promise.when(promises);

    }).then(function () {
        var installationQuery = new Parse.Query("_Installation");
        var query = new Parse.Query("_User");
        query.doesNotMatchKeyInQuery("wotNickname", "wotNickname", installationQuery);
        return query.find();
    }).then(function (orphans) {

        if (typeof (orphans) === undefined || orphans.length === 0) {
            return Parse.Promise.as();
        }

        status.message("Processing users");
        userNumber = orphans.length;
        var promises = orphans.map(function (user) {
            user.unset("active");
            return user.save();
        });
        return Parse.Promise.when(promises);
    }).then(function () {
        status.success(installationNumber + " installations updated and " + userNumber + " users deleted successfully");
    }, function (error) {
        status.error(error);
    });
});

Parse.Cloud.afterSave("IngameCounter", function (request) {
    "use strict";
    var query = new Parse.Query("GlobalIngameCounter");
    return query.first().then(function (globalIngameCounter) {
        if (!globalIngameCounter) {
            globalIngameCounter = new Parse.Object("GlobalIngameCounter");
        }
        globalIngameCounter.increment(request.object.get("game"));
        return globalIngameCounter.save();
    });
});

var http = require('http');
var server = http.createServer(function (req, res) {
    var ua = req.headers["user-agent"];
    var checker = {
        ios: ua.match(/(iPhone|iPod|iPad)/),
        blackberry: ua.match(/BlackBerry/),
        android: ua.match(/Android/),
        windows: ua.match(/IEMobile/)
    };
    var destination = "/index.html";
    if (checker.android) {
        destination = "https://play.google.com/store/apps/details?id=com.appgranula.ingame";
    } else if (checker.ios) {
        destination = "https://itunes.apple.com/app/ingame/id910864040";
    }
    res.writeHead(301, {
        'Location': destination
    });
    res.end();
});
server.listen();