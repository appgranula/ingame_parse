$(document).ready(function(){
    adjustHeaderHeight();
    $(window).resize(adjustHeaderHeight);
});

function viewport () {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}

function adjustHeaderHeight() {
    var top = $("#title-block");
    var bottom = $("#platforms-block");
    var title = $("#title");
    var desc = $("#description");
    var wHeight = viewport().height;
    var topHeight = 452;
    var topPadding = 125;
    if (wHeight > 768) {
        topHeight = wHeight * 0.62;
        topPadding = (topHeight - (title.height() + desc.height() + 24))/2
    }
    top.css("height", topHeight);
    top.css("paddingTop", topPadding);
}